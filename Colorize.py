#coding:UTF-8

import sys
import numpy as np

master_vertices = []
facets = []
v_u = []
f_u = []

color_vertices = []
colorNum = 0

readerFlag = 0

def load(_name, vertices, masterFlag):
	global readerFlag

	parts = _name.split(".")
	if parts[1] == "vtk":
		try:
			f = open(_name, "r")
		except IOError:
			print("cannot open file: " + _name + "\n")
			return False

		while True:
			line = f.readline()
			if not line:
				break
			processLineVTK(line, vertices, masterFlag)
		f.close()
		readerFlag = 0
		return True

	elif parts[1] == "obj":
		try:
			f = open(_name, "r")
		except IOError:
			print("cannot open file: " + _name + "\n")
			return False

		while True:
			line = f.readline()
			if not line:
				break
			processLineOBJ(line, vertices, masterFlag)
		f.close()
		return True

def processLineVTK(line, vertices, masterFlag):
	global readerFlag

	parts = line.split(" ")
	if len(parts) == 0:
		return False
	if parts[0] == "POINTS":
		readerFlag = 1
		return True
	if parts[0] == "CELLS":
		readerFlag = 2
		return True
	if parts[0] == "CELL_TYPES":
		readerFlag = 0
		return True

	if readerFlag == 1:
		tmp = np.array([float(parts[0]), float(parts[1]), float(parts[2])])
		vertices.append(tmp)
		return True

	if readerFlag == 2 and masterFlag == 1:
		#f_parts = line.split("	")
		v_id = [int(parts[1]), int(parts[2]), int(parts[3])]
		facets.append(v_id)
		return True


def processLineOBJ(line, vertices, masterFlag):
	parts = line.split(" ")
	if len(parts) == 0:
		return False
	
	if parts[0] == "v":
		tmp = np.array([float(parts[1]), float(parts[2]), float(parts[3])])
		vertices.append(tmp)
		return True
	
	if parts[0] == "f" and masterFlag == 1:
		v_id = [int(parts[1])-1, int(parts[2])-1, int(parts[3])-1]
		facets.append(v_id)
		return True

def outputVTK(_name):
	output_filename = _name + "_colored.vtk"
	output = open(output_filename, mode="w", encoding="ascii")
	output.write("# vtk DataFile Version 2.0\n")
	output.write("colored_vtk\n")
	output.write("ASCII\n")
	output.write("DATASET UNSTRUCTURED_GRID\n")
	output.write("POINTS {0} float\n".format(len(master_vertices)))

	for i in range(len(master_vertices)):
		tmp_v = master_vertices[i]
		output.write("{0} {1} {2}\n".format(tmp_v[0], tmp_v[1], tmp_v[2]))

	output.write("CELLS {0} {1}\n".format(len(facets), 4*len(facets)))

	for i in range(len(facets)):
		tmp_f = facets[i]
		output.write("3 {0} {1} {2}\n".format(tmp_f[0], tmp_f[1], tmp_f[2]))

	output.write("CELL_TYPES {0}\n".format(len(facets), 4*len(facets)))

	for i in range(len(facets)):
		output.write("5\n")
	
	output.write("CELL_DATA {0}\n".format(len(facets)))
	output.write("SCALARS cell_scalars float\n")
	output.write("LOOKUP_TABLE default\n")

	for i in range(len(facets)):
		output.write("{0}\n".format(f_u[i]))

	output.close()


def colorize(_color_vertices, _colorNum):
	for i in range(len(master_vertices)):
		for j in range(len(_color_vertices)):
			if np.allclose(master_vertices[i], _color_vertices[j]):
				v_u[i] = _colorNum

def setFacetsU():
	for i in range(len(facets)):
		v_id = facets[i]
		u0 = v_u[v_id[0]]
		u1 = v_u[v_id[1]]
		u2 = v_u[v_id[2]]

		if (u0 == u1) and (u1 == u2):
			f_u[i] = u0
		else:
			pass

#main
if __name__ == '__main__':
	master_name = input("色を付けたいメッシュの名前は？ (vtkとobjが読み込めます)：　")
	load(master_name, master_vertices, 1)

	for i in range(len(master_vertices)):
		v_u.append(-1)
	
	for i in range(len(facets)):
		f_u.append(-1)
	
	
	while True:
		answer = input("まだ着色を続けますか？(Y/n): ")
	
		if answer == "Y":
			color_area = input("色を塗りたい領域のファイルの名前は？　(vtkとobjが読み込めます)：　")
			load(color_area, color_vertices, 0)
	
			colorNum += 1
			colorize(color_vertices, colorNum)
			color_vertices.clear()
			setFacetsU()
	
		elif answer == "n":
			break
	
		else:
			pass

	outputVTK(master_name)	